# How to Package a Rust Crate for Debian

# Add Name and Email to .bashrc
```
DEBEMAIL="my.email@example.com"
DEBFULLNAME="First-name Last-name"
CHROOT=debcargo-unstable-amd64-sbuild
export DEBEMAIL DEBFULLNAME CHROOT
```

# Clone Debian Rust Team Repository
`git clone https://salsa.debian.org/rust-team/debcargo-conf.gi`
`
# Navigate to the Repository
`cd debcargo-con`

# Run update.sh for Crate Information
`./update.sh crate-nam`

# Navigate to Debian Directory
`cd src/crate-name/debian`

# Copy Copyright Hint
`cp copyright.debcargo.hint copyrigh`

# Fix Copyright Information
# Edit the copyright file to ensure accurate copyright information.

# Add Dependencies to debcargo.toml
# Update debcargo.toml to specify dependencies on native packages.
# [packages.lib]
# depends = ["libyaml-dev"]

# Setup Build Environment
# Install required packages and create a chroot environment.
```
sudo apt install devscripts reprepro debootstrap sbuild dh-cargo
```
```
sudo sbuild-createchroot --include=eatmydata,ccache,gnupg,dh-cargo,cargo,lintian,perl-openssl-defaults \
      --chroot-prefix debcargo-unstable unstable \
      /srv/chroot/debcargo-unstable-amd64-sbuild http://deb.debian.org/debian
```

# Perform Test Build
`cd ../../../build && ./build.sh crate-nam`

# Add Request for Sponsorship (RFS) File
# Create a file named RFS in the Debian directory.
`touch ../src/crate-name/debian/RF`

# Commit Changes to Branch
```
cd ..
git branch package-crate-nam
git checkout package-crate-nam
git add 
git commit -m "package version X.Y.Z of crate-name
```
# Push Changes to Git Repository
```
git push git@salsa.debian.org:capitol-guest/debcargo-conf.git
```
# Join IRC Channel #debian-rust
# Connect to the #debian-rust channel on irc.oftc.net to seek assistance and guidance from experienced members.

# The #debian-rust community is helpful and can provide support throughout the packaging process.
